var success = (results, message) => {
    return {
        success: true,
        message: message,
        result: results
    }
}

var error = (err) => {
    return{
        success: false,
        message: err
    }
}

module.exports = { success, error }
