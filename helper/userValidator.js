const Joi = require('@hapi/joi');

function validationJoi(userSchema) {
    const schema = {
        email: Joi.string().required(),
        password: Joi.string().required().min(6).max(12)
    }
    return Joi.validate(userSchema, schema);
    }
module.exports =  validationJoi ;
