const jwt = require('jsonwebtoken');
const { success, error } = require('./resFormatter.js');

module.exports = (req, res, next) => {
    const token = req.header('authorization');
    if (!token) return res.status(401).json( error('Access denied!') )

    const spt = token.split(" ");

    try {
        const verified = jwt.verify(spt[1], process.env.DBLOGIN);
        req.user = verified
        console.log(req.user)
        next();
    }
    catch {
        res.status(422).json( error('Invalid token!') )
    }
}
