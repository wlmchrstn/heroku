const user = require('../models/userModel.js');
const validator = require('../helper/userValidator.js');
const bcrypt = require('bcrypt');
const salt = 10;
const jwt = require('jsonwebtoken');
const { success, error } = require('../helper/resFormatter.js');
const _ = require('lodash');

module.exports = {

    async create(req,res) {
        const { error } = validator(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    let User = await user.findOne({ email: req.body.email });
    if (User) return res.status(400).send(`${req.body.email} is already registered, Please use other email`);

    let pwd = req.body.password
    bcrypt.hash(pwd, salt, function(err,hash) {
            user.create({
                email: req.body.email,
                password: hash
            })
            .then(result => {
                res.status(201).json( success(result,'User created!') )
            })
            .catch(err => {
                res.status(422).json( error('Failed to create user!') )
            });
        });
    // User = new user(req.body)
    },

    login(req,res) {
        user.findOne({email: req.body.email})
        .then(data => {
            let pwd = req.body.password;
            let hash = data.password;
            bcrypt.compare(pwd, hash, function(err, result) {
            if (result == true) {
                let token = jwt.sign({ _id: data._id }, process.env.DBLOGIN);
                res.status(200).json( success(token,'Token created!') )
            }
            else {
                res.status(422).json( error("Incorrect password!") );
            }
        })
        })
        .catch(err => {
            res.status(422).json( error("User not found!") )
        })
    },

    showAll(req,res) {
        user.find({})
        .then(result => {
            console.log(result);
            return res.status(200).json( success(result, 'All user showed!') );
        })
        .catch(err => {
            return res.status(422).json( error('No user!') )
        });
    },

    show(req,res) {
        user.findById(req.user)
        .then(result => {
            console.log(result);
            return res.status(200).json( success(_.pick(result)) );
        })
        .catch(err => {
            return res.status(422).json( error('User not found!') );
        });
    },

    delete(req, res) {
        user.findByIdAndDelete(req.user)
        .then(result => {
            console.log(result);
            return res.status(200).json( success('User deleted!') );
        })
        .catch(err => {
            return res.status(422).json( error('User not found!') );
        });
    }
}
