const post = require('../models/postModel.js');
const user = require('../models/userModel.js');
const { success, error } = require('../helper/resFormatter.js');

module.exports = {
    createUserPost(req,res) {
        post.create(req.body)
        .then(result => {
            user.findById(req.user._id, (err, data) => {
                data.posts.push(result)
                data.save();
                console.log(result)
                result.users=req.user
                result.save((err, hasil) => {
                    res.status(201).json( success(hasil, "Post created!") )
                });
            });
        })
        .catch(err => {
            return res.status(422).json( error("Can't create post!") );
        })
    },

    getUserPost(req,res) {
        user.findById(req.user._id)
        .populate('posts')
        .then(result => {
            console.log(result);
            return res.status(200).json( success(result) );
        })
        .catch(err => {
            return res.status(422).json( error('No post found!') );
        });
    },
    
    updateUserPost(req,res) {
        post.findById(req.params.id)
        .then(data => { 
            console.log(data); 
            if (data.users != req.user._id) {
                return res.status(422).json( error('Invalid user! Authentication needed!') )
            }
            
            post.findByIdAndUpdate(req.params.id, req.body, function(err, update) {
                if (err) return res.status(422).json( error("Can't update post!") )
                res.status(200).json( success(update, "Post updated!") )
            })
        })
        .catch(err => {
            return res.status(422).json( error("No post found!") );
        });
    },

    deleteUserPost(req, res) {
        post.findById(req.params.id).populate('users', '_id')
        .exec((err, data) => {
            if (data.users._id != req.user._id) {
                return res.status(422).json( error('Invalid user! Authentication needed!') )
            }
            console.log(data)
            
            post.findByIdAndDelete(data._id, function(err, deleted) {
                if (err) return res.status(404).json( error("No post to delete!") )
                user.updateOne(
                    { _id: req.user._id },
                    { $pullAll: { posts: [ req.params.id ] } },
                    { safe: true, multi: true},
                    function(err, obj) {
                        if (err) return res.status(422).json( error("Can't deleted User's post!") )
                        res.status(200).json( success("Post deleted!") )
                    }
                )
            })
        })
    }
}
