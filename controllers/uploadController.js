const multer = require('multer');
const { success, error } = require('../helper/resFormatter.js');
const cloudinary = require('cloudinary').v2;
const Datauri = require('datauri');
const dUri = new Datauri();
const user = require('../models/userModel.js');

// const storage = multer.diskStorage({
//     destination: function(req, file, cb) {
//         cb(null, 'public')
//     },
//     filename: function (req, file, cb) {
//         console.log(file);
//         var filetype = '';
//         if (file.mimetype === 'image/gif') {
//             filetype = 'gif';
//         }
//         else if (file.mimetype === 'image/png') {
//             filetype = 'png';
//         }
//         else if (file.mimetype === 'image/jpeg') {
//             filetype = 'jpg';
//         }
//         cb(null, file.fieldname + '-' + Date.now() + '-' + filetype )
//     }
// })

cloudinary.config({
    api_key: "524832772999249",
    api_secret: "PY0AeFB7C_hw8YhY43pZb9hLK6E",
    cloud_name: "wlmchrstn"
})
const uploader = multer().single('avatar')

module.exports =

    function(req,res) {
        uploader(req, res, err => {
            var file = req.file
            console.log(file)
            if(!file) {
                return res.status(422).json( error('Please insert file!') )
            }
            var ava = dUri.format(`${req.file.originalname}-${Date.now()}`, req.file.buffer);
            cloudinary.uploader.upload(ava.content)
            .then(data => {
                console.log(data)
                user.findByIdAndUpdate(req.user, {
                    avatar: data.secure_url
                }, function(err, result) {
                    if (err) return res.status(422).json( error("Can't update avatar!") )
                    res.status(200).json( success(result, "Avatar updated!") )
                })                
            })
            .catch(err => {
                res.status(422).json( error("Can't upload file") )
            })
        })
    }
