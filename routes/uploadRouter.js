const router = require('express').Router();
const uploadRouter = require('../controllers/uploadController.js')
const auth = require('../helper/userAuth.js');

router.post('/', auth, uploadRouter);

module.exports = router;
