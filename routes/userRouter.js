const router = require('express').Router();
const userRouter = require('../controllers/userController');
const auth = require('../helper/userAuth.js');

router.post('/create', userRouter.create);
router.get('/', userRouter.showAll);
router.get('/show', auth, userRouter.show);
router.delete('/delete', auth, userRouter.delete);
router.post('/login', userRouter.login);

module.exports = router;
