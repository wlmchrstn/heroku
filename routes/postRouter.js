const router = require('express').Router();
const postRouter = require('../controllers/postController');
const auth = require('../helper/userAuth.js');

router.post('/', auth, postRouter.createUserPost);
router.get('/', auth, postRouter.getUserPost);
router.put('/', auth, postRouter.updateUserPost);
router.delete('/', auth, postRouter.deleteUserPost);

module.exports = router;
