const router = require('express').Router();
const userRouter = require('./userRouter.js');
const postRouter = require('./postRouter.js');
const uploadRouter = require('./uploadRouter.js');

router.use('/user', userRouter);
router.use('/user/post', postRouter);
router.use('/upload', uploadRouter)

module.exports = router;
