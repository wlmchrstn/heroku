const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const mongoose = require('mongoose');
const swaggerUi = require('swagger-ui-express');
const swaggerDoc = require('./swagger.json');
const env = process.env.NODE_ENV || 'development'
const router = require('./routes/index.js');  

if (env == 'development' || env == 'test') {
    const dotenv = require('dotenv').config();
};

const dbConfig = {
    development: process.env.DBDEV,
    test: process.env.DBTEST,
    user: process.env.DBLOGIN
}
console.log(dbConfig[env])

mongoose.connect(dbConfig[env], { useNewUrlParser: true, useCreateIndex: true});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc))
app.use('/api', router);

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.status(200).json({
        success: true,
        message: "Welcome to API!"
    });
});

app.listen(port, () => {
    console.log(`Server Started at ${Date()}!`);
    console.log(`Listening on port ${port}!`);
});

module.exports = app;
